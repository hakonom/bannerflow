# Bannerflow

Workflow for automating HTML banner production tasks using Gulp.


## Getting started

This workflow requires Node.js. The easiest way to install node on a mac is with Homebrew: `brew install node`, or you can go to [nodejs.org](https://nodejs.org).

Install the project dependencies with `npm install`.


### Folder structure

Things will only work if you use the intended structure and naming conventions.

- Name the project folder something like this: `Client-Campaign`
This is the folder where this file (README.md) is placed. This folder name will be used for naming titles in manifest files and the final zip files.
- Name the banner folders with dimensions like this: `300x250`, `580x400`
The banner folder names will be used as suffixes in the manifest and zip files.

(The full names will be e.g. Client-Campaign-300x250).


### Creating banners

The project comes with an example banner (300x250), which you can use as a starting point.

- The banner HTML file is placed in banner root and must be named `index.html`.
- Sass(/CSS) goes in the `style.scss` file. If you want to use partials, import them into style.scss.
- Put JavaScript files wherever, and use the attribute `inline` when referencing the scripts in the HTML file. This embeds them inline into the file. You can also write JavaScript directly into the HTML file. Referenced scripts without the inline attribute will NOT be copied into the final banners. You can however reference external scripts with absolute links, but consider embedding them to keep the number of requests down.
- PNG files will be optimized automatically, just save bloated 24-bit files from Photoshop.
JPG and GIF files will not be optimized (Photoshop does a decent job with these file types and allows you more control).


## Serve

When developing, you can "serve" the banner you're working on, like this: `gulp serve --banner 300x250`. What this does is fire up a web server in your default browser that will automatically reload as you do changes to the banner. Use with the option `--banner` followed by the banner name. You can also run `gulp serve` without the banner option to serve the whole project, allowing you to browse to the desired banners (serving the whole project will be slower when you have a lot of banners). To exit the serve command in the Terminal, press `Ctrl + C`.


## Build

Build final banners using `gulp build`. Without options it builds all banners, but you can also specify a single one with the "banner" option: `gulp build --banner 300x250`. See more options below.

### Options

All options are optional.

- `banner` - specify a single banner to perform the given task to. Example: `gulp build --banner 300x250`.
- `separator` – separator character to use in zip file name and manifest title. Default: "-". Example: `gulp build --separator _`
- `suffix` - text to append to end of zip file name and manifest title, e.g. version number or variant. Example: `gulp build --suffix v3`

Example with multiple options: `gulp build --banner 300x250 --separator _ --suffix v3_blue`.



The build task does the following:

- Removes previously built files
- Compiles Sass, autoprefixes CSS
- Places CSS and JavaScript files inline in the HTML file
- Minifies the HTML-file including inlined CSS and JavaScript
- Optimizes PNG-files
- Writes manifest.json using folder names for titles and dimensions
- Compresses the banners to Adform-compatible zip-files
- Creates a preview file with the final banners

The final zip files will be in banners/\_levering.
The preview will be in banners/\_preview.


### Watch without serve

If, for some reason, you just want to watch the files for changes to compile Sass automatically but not run the web server, use `gulp watch`. This also works with the `banner` option, e.g. `gulp watch --banner 300x250`. You can then check the files in a way you want.

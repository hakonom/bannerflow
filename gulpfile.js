"use strict";

// Dependencies

var gulp = require("gulp"),
    argv = require("yargs").argv,
    autoprefixer = require("gulp-autoprefixer"),
    browserSync = require("browser-sync").create(),
    del = require("del"),
    fs = require("fs"),
    htmlmin = require("gulp-htmlmin"),
    imagemin = require("gulp-imagemin"),
    inlineSource = require("gulp-inline-source"),
    merge = require("merge-stream"),
    path = require("path"),
    pngquant = require("imagemin-pngquant"),
    rename = require("gulp-rename"),
    sass = require("gulp-sass"),
    size = require("gulp-size"),
    zip = require("gulp-zip");


// Global vars

var bannerPath = "banners/";
var browserSupport = "last 10 versions"; // Define desired CSS browser support, autoprefixer uses this. Documentation: https://github.com/browserslist/browserslist#queries
var pngquantOptions = { // PNG quality settings. Ref: https://github.com/imagemin/imagemin-pngquant
  // floyd: 0.5, // level of dithering from 0-1, default 0.5
  // nofs: false, // disable Floyd-Steinberg dithering, default false
  // posterize: 0, // Reduce precision of the palette by number of bits. Values 0-4
  // speed: 3, // speed/quality trade-off, default 3
  quality: "50-90", // use the minimum amount of colors to meet the minimum quality defined here
  verbose: true, // verbose output
  strip: true, // remove optional metadata
  // input: // buffer or stream to optimize
}

var defaultSeparator = "-"; // The default separator character to use in filenames and titles, if not passed by user
var errorColor = "\x1b[31m" + "%s" + "\x1b[0m"; // color used for displaying errors in console. First part is the error color, second represents the string (second parameter of console.log() that gets injected), and last part resets console color).


// Global functions

// function to get the subfolders in a given directory, returns an array.
function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

// get campaign/project folder name, used for zip files and titles in manifest.json
function getCampaignFolderName() {
  var campaign = __dirname;
  campaign = __dirname.split("/");
  campaign = campaign[campaign.length - 1];
  return campaign;
}

// function for checking that a folder exists. If second parameter is not passed (or false), create the folder. Pass second parameter (anything or true) to perform only a check and not create the folder
function checkFolderExist(folder,checkonly) {
  if (fs.existsSync( folder )) {
    return true;
  } else {
    if (!checkonly) { // check that checkfolder parameter is not passed
      fs.mkdirSync( folder );
    }
  }
}

// function for reading a file asynchronously. Pass file, charset and callback function
function readFileAsync(file, charset, callback) {
  fs.readFile(file, charset, function(err, content) {
    if (err) {
      return callback(err);
    }
    callback(null,content);
  });
}

/* ----------------
    Gulp tasks
---------------- */


// Clean task

gulp.task("clean", function() {
  if (argv.banner) {
    return del([
      bannerPath + argv.banner + "/css/",
      bannerPath + argv.banner + "/dist/",
      bannerPath + "_levering" + getCampaignFolderName() + "-" + argv.banner + ".zip",
      bannerPath + "_preview/" + argv.banner,
    ]);
  } else {
    return del([
      bannerPath + "**/css/",
      bannerPath + "**/dist/",
      bannerPath + "_levering",
      bannerPath + "_preview",
    ]);
  }
});


// Compile Sass

gulp.task("compileSass", function() {

  if (argv.banner) { // check if banner is being passed

    return gulp.src(bannerPath + argv.banner + "/style.scss")
     .pipe(sass())
     .pipe(autoprefixer(browserSupport))
     .pipe(gulp.dest(bannerPath + argv.banner + "/css"));

  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      return gulp.src(path.join(bannerPath, folder, '/style.scss'))
        .pipe(sass())
        .pipe(autoprefixer(browserSupport))
        .pipe(gulp.dest(bannerPath + folder + "/css"));
    });
    return merge(tasks);

  }
});


// Inline CSS, JS, img srcs

gulp.task("inlineSource", ["compileSass"], function() {

  if (argv.banner) { // check if banner is being passed

    return gulp.src( bannerPath + argv.banner + "/index.html")
    .pipe(inlineSource())
    .pipe(gulp.dest(bannerPath + argv.banner + "/dist/"));

  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      return gulp.src( bannerPath + folder + "/index.html")
      .pipe(inlineSource())
      .pipe(gulp.dest(bannerPath + folder + "/dist/"));
    });
    return merge(tasks);

  }
});


// Minify HTML (including CSS & JS)

gulp.task("minify", ["inlineSource"], function() {

  if (argv.banner) { // check if banner is being passed

    return gulp.src(bannerPath + argv.banner + "/dist/index.html")
    .pipe( htmlmin({
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
    }) )
    .pipe(gulp.dest( bannerPath + argv.banner + "/dist/"))

  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      return gulp.src(bannerPath + folder + "/dist/index.html")
      .pipe( htmlmin({
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
      }) )
      .pipe(gulp.dest( bannerPath + folder + "/dist/"))
    });
    return merge(tasks);

  }

  // clean up, remove css
  del(bannerPath + folder)
});


// Optimize images

gulp.task('optimizeImages', function() {

  // Optimize PNGs using imagemin and imagemin-pngquant
  // NOTE: New way of setting options since imagemin 3.0
  // ref. https://github.com/sindresorhus/gulp-imagemin/releases/tag/v3.0.0
  // The new syntax: imagemin([plugins{plugin-options}], {options})

  if (argv.banner) { // check if banner is being passed

    return gulp.src( bannerPath + argv.banner + "/*.png" )
    .pipe(imagemin([
        // plugins to use (array)
        pngquant( pngquantOptions ),  // pngquant-specific options, defined at top of file
      ],
      {
        // options (general for imagemin)
        verbose: true,
      }
    ))
    .pipe(gulp.dest( bannerPath + argv.banner + "/dist/" ));

  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      return gulp.src( bannerPath + folder + "/*.png" )
      .pipe(imagemin([
          // plugins to use (array)
          pngquant( pngquantOptions ),  // pngquant-specific options, defined at top of file
        ],
        {
          // options (general for imagemin)
          verbose: true,
        }
      ))
      .pipe(gulp.dest( bannerPath + folder + "/dist/"));
    });
    return merge(tasks);

  }
});


// Move images

gulp.task("moveImages", function() {
  // move jpg and gif only – png will be handled by optimizeImages()

  if (argv.banner) { // check if banner is being passed

    return gulp.src([
      bannerPath + argv.banner + "/*.jpg",
      bannerPath + argv.banner + "/*.jpeg",
      bannerPath + argv.banner + "/*.gif"
    ])
    .pipe(gulp.dest( bannerPath + argv.banner + "/dist/"))

  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      return gulp.src([
        bannerPath + folder + "/*.jpg",
        bannerPath + folder + "/*.jpeg",
        bannerPath + folder + "/*.gif"
      ])
      .pipe(gulp.dest( bannerPath + folder + "/dist/"))
    });
    return merge(tasks);
  }

});


// Write manifest.json

gulp.task("writeManifest", function() { // wait for moveImages to be sure the dist folder is created

  var campaign = getCampaignFolderName();

  // set separator
  if (argv.separator) {
    var separator = argv.separator; // get separator character if passed by user,
  } else {
    separator = defaultSeparator; // or use the default defined as global var
  }

  // set suffix
  if (argv.suffix) {
    var suffix = separator + argv.suffix;  // get suffix if passed by user, prefixed with separator,
  } else {
    var suffix = ""; // or set it empty
  }

  // function for inserting campaign and banner format into manifest
  function editManifest(manifest,campaign,banner) {
    manifest = JSON.parse(manifest);
    manifest.title = campaign + separator + banner + suffix;
    manifest.width = banner.split("x")[0];
    manifest.height = banner.split("x")[1];

    writeManifestFile(banner, manifest);
  }

  // function for writing updated manifest to file
  function writeManifestFile(banner, manifest) {

    checkFolderExist( bannerPath + banner + "/dist" );

    return fs.writeFileSync(
      bannerPath + banner + "/dist/manifest.json",
      JSON.stringify(manifest, null, 2) // third parameter in stringify prettifies with spacing level 2
    );

  }

  // function for reading manifest.json
  function readManifest(banner) {
    // read manifest.json using readFileAsync function defined at top of file. Pass file, charset and callback
    readFileAsync( "templates/manifest.json", "utf8", function(err,content) {
      if (err) { console.log(err); }
      editManifest(content,campaign,banner);
    });
  }



  if (argv.banner) { // check if banner is being passed
    // check that passed banner exists
    if (checkFolderExist(bannerPath + argv.banner,true)) { // pass second parameter true to perform check-only
      readManifest(argv.banner);
    } else {
      return console.log(errorColor,"Can't find banner. Please check that it exists.");
    }


  } else { // no banner passed, use all

    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(banner) {
      readManifest(banner); // call readManifest for each banner
    });

  }

});


// Watch

gulp.task("watch", function() {
  if (argv.banner) {
    gulp.watch( bannerPath + argv.banner + "/**/**.scss", ["compileSass"]);
  } else {
    gulp.watch( bannerPath + "/**/**.scss", ["compileSass"]);
  }
});


// Serve

gulp.task("serve", ["compileSass"], function() {

  var serverPath;

  if (argv.banner) { // check if banner is being passed
    browserSync.init({
      server: bannerPath + argv.banner
    });

  } else { // no banner passed, serve the root folder with directory option (directory listing / indexes) true
    browserSync.init({
      server: bannerPath,
      directory: true
    });
  }

  // start regular watch task
  gulp.start("watch");

  // watch compiled files
  gulp.watch([
    bannerPath + "**/index.html",
    bannerPath + "**/css/style.css",
    bannerPath + "**/js/**/**.js",
  ]).on('change', browserSync.reload);

});


// Preview

gulp.task("preview", function() {

  // this will be the iframes for the banners
  var bannerMarkup = "";

  // function for writing the preview HTML file
  function createHtml(header, footer) {
    checkFolderExist( bannerPath + "_preview" );
    fs.writeFileSync( bannerPath + "_preview/index.html", header + bannerMarkup + footer );
  }

  // function for creating the actual HTML markup
  function createBannerMarkup(banner) {
    var figure =
    "   <figure>" + "\n" +
    "     <iframe src='" + banner + "/index.html" + "' width ='" + banner.split("x")[0] + "' height='" + banner.split("x")[1] + "' frameborder='0' scrolling='no'></iframe>" + "\n" +
    "     <figcaption>" + banner + "</figcaption>" + "\n" +
    "   </figure>" + "\n"
    bannerMarkup += (figure);
  }

  // call createBannerMarkup, passing banner as argument
  if (argv.banner) { // check if banner is being passed
    createBannerMarkup(argv.banner);
  } else { // no banner passed, use all and call several times
    var banners = getFolders(bannerPath);
    var moveFiles = banners.map(function(folder) {
      createBannerMarkup(folder);
    });
  }

  // read preview header and footer using readFileAsync function defined at top of file. Pass file, charset and callback
  readFileAsync( "templates/preview-header.html", "utf8", function(err,header) { // read header
    if (err) { console.log(err); }
    readFileAsync( "templates/preview-footer.html", "utf8", function(err,footer) { // read footer
      if (err) { console.log(err); }
      createHtml(header, footer); // call createHtml passing preview header and footer
    });
  });

  // move all dist content from banners to the preview folder
  var banners = getFolders(bannerPath);
  var moveFiles = banners.map(function(folder) {
    return gulp.src(bannerPath + folder + "/dist/**/**.*", { base: bannerPath + folder + "/dist/" })
    .pipe(gulp.dest(bannerPath + "_preview" + "/" + folder));
  });

  return merge(moveFiles);

});


// Serve Preview

gulp.task("servePreview", ["preview"], function() {

  // serve the preview
  function servePreview() {

    // Get process.stdin as the standard input object.
    var standard_input = process.stdin;

    // Set input character encoding.
    standard_input.setEncoding('utf-8');

    // Prompt user to input data in console.
    console.log(
      " ---------------------------------------\n" +
      " All tasks done.\n" +
      " Preview in browser? y/n"
    );

    // When user input data and click enter key.
    standard_input.on('data', function (data) {

        // User input exit.
        if(data === "y\n"){
          console.log(
            " Serving preview on localhost. Press Ctrl + C to exit.\n" +
            " ---------------------------------------"
          );
          browserSync.init({
            server: bannerPath + "_preview/"
          });
        }else if(data === 'n\n'){
          // Program exit.
          console.log(
            " OK, champ. Just get the zip files from '/_levering'\n" +
            " ---------------------------------------"
          );
          process.exit();
        }
    });
  }

  setTimeout(function() {
    servePreview();
  },100);

});


// Compress to impress.

gulp.task("compress", function() {

  // set separator
  if (argv.separator) {
    var separator = argv.separator; // get separator character if passed by user,
  } else {
    separator = defaultSeparator; // or use the default defined as global var
  }

  // set suffix
  if (argv.suffix) {
    var suffix = separator + argv.suffix;  // get suffix if passed by user, prefixed with separator,
  } else {
    var suffix = ""; // or set it empty
  }

  var banners = getFolders(bannerPath);
  var tasks = banners.map(function(folder) {
    return gulp.src(bannerPath + folder + "/dist/**/**.*", { base: bannerPath + folder + "/dist/"})
    .pipe(size({
      title: folder + " total size (uncompressed):",
    }))
    .pipe(zip( getCampaignFolderName() + separator + folder + suffix + ".zip"))
    .pipe(gulp.dest(bannerPath + "_levering/"));
  });

  return merge(tasks);

});


// Prepare build (make sure clean is done before continuing)

gulp.task("prepareBuild", ["clean"], function() {
  gulp.start("buildTasks");
});


// Post build (clean up temp files)
gulp.task("cleanTemp", function() {
  if (argv.banner) {
    del(bannerPath + argv.banner + "/css"); // delete temp css folder/file
  } else {
    var banners = getFolders(bannerPath);
    var tasks = banners.map(function(folder) {
      del(bannerPath + folder + "/css"); // delete temp css folder/file
    });
  }
})


// Build tasks

gulp.task("buildTasks", ["minify", "optimizeImages", "moveImages", "writeManifest"], function() {
  gulp.start("compress");
  gulp.start("servePreview");
  gulp.start("cleanTemp");
});


// Build

gulp.task("build", ["prepareBuild"], function() {

});


// Default

gulp.task("default", ["build"]);
